package CRUD;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class StergeBilet {
    private JPanel Container;
    private JButton deleteButton;
    private JButton cancelButton;
    private JTextField numarInput;
    private JPanel panel1;

    public StergeBilet() {
        JFrame frame = new JFrame("StergeBilet");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        deleteButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String numar = numarInput.getText();
                int nr = Integer.parseInt(numar);

                int tag = 7;
                String query;
                query = "DELETE FROM tickets WHERE idtickets = " + nr + ";";
                Client cl = new Client();
                try {
                    if (cl.connectServer(tag, query).equals("Biletul a fost sters!")){
                        frame.dispose();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
