package CRUD;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import Code.Client;

public class AfisareBileteSala {
    private JPanel Container;
    private JButton afiseazaButton;
    private JButton cancelButton;
    private JTextField salaInput;
    private JPanel panel1;

    public AfisareBileteSala() {

        JFrame frame = new JFrame("AfiseazaBilet");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        afiseazaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String sala = salaInput.getText();
                int nr = Integer.parseInt(sala);
                String query;
                query = "SELECT * FROM tickets WHERE id_sala = " + nr + ";";
                Client cl = new Client();
                int tag = 11;
                try {
                    cl.connectServer(tag,query);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
